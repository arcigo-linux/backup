#!/bin/bash

##########################################################################
# Program	 : connect-labs
# Description: This script allows user to connect labs
# Maintainer : Arcigo <arcigo.linux@gmail.com>
# Version	 : 1.0
##########################################################################
#

conf_lc="${HOME}/.config/wg.txt"

# It generates config
function _create_config() {
	while true; do
		if [[ ! -f $conf_lc ]]; then
			mkdir ${HOME}/.config
			touch $conf_lc
		elif [[ -f $conf_lc ]]; then
			echo "$1" >> $conf_lc
			echo "$2" >> $conf_lc
			break
		fi
	done
}

# Getting username and ip from user
function _get_info() {
	
	while true; do
	    read -p "[?] Enter the username: " username
	    if [ $username ]; then
	    	export UR_NAME="$username"
	        break
	    else
	        echo -e "[-] Invalid Username\n"
	    fi
	done
	echo
    while true; do
        read -p "[?] Enter the Lab's IP: " labs_ip
        if [[ "$labs_ip" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
            export UR_IP="$labs_ip"
            break
        else
            echo -e "[-] Invalid IP! Try again\n"
        fi
    done
    echo ""
}

# Main thing to first
# This function writes data to config
function check_info() {
	local junk
	_get_info

	read -p "[?] Is this information correct or not? [Y/n] " junk

	if [[ $junk == "" ]]; then
		echo -e "\n[*] Creating config ...\n" && sleep 1
		_create_config ${UR_NAME} ${UR_IP}
		echo -e "[*] Config created!\n"
	elif [[ $junk == "Y" || $junk == "y" ]]; then
		echo -e "\n[*] Creating config ...\n" && sleep 1
		_create_config ${UR_NAME} ${UR_IP}
		echo -e "[*] Config created!\n"
	elif [[ $junk == "N" || $junk == "n" ]]; then
		echo -e "\n[!] Type the creds carefully!\n"
		_get_info
		echo -e "\n[*] Creating config ...\n" && sleep 1
		_create_config ${UR_NAME} ${UR_IP}
		echo -e "[*] Config created!\n"
	fi
}

# Getting data from config
function _getting_creds_from_config() {
	local grep_user grep_ip

	if [[ ! -d ${HOME}/.config ]]; then
		mkdir ${HOME}/.config
		if [[ ! -f $conf_lc ]]; then
			touch $conf_lc
		fi
	fi

	grep_user=$(cat $conf_lc | head -n 1)
	grep_ip=$(cat $conf_lc | tail -n 1)
		
	if [[ $grep_user == "" && $grep_ip == "" ]]; then
		check_info
	elif [[ $grep_user == "" ]]; then
		check_info
	elif [[ $grep_ip == "" ]]; then
		check_info
	else
		echo $grep_user > /tmp/wg-tmp.conf
		echo $grep_ip >> /tmp/wg-tmp.conf
	fi
}

function ssh_labs() {
	local user ip junk

	read -p "[?] Want to connect to labs? [Y/n] " junk

	user=$(cat /tmp/wg-tmp.conf | head -n 1)
	ip=$(cat /tmp/wg-tmp.conf | tail -n 1)

	if [[ $junk == "" ]]; then
		echo "[+] SSHing into ${user}@${ip}"
		ssh ${user}@${ip}
	elif [[ $junk == "y" || $junk == "Y" ]]; then
		echo "[+] SSHing into ${user}@${ip}"
		ssh ${user}@${ip}
	else
		echo "[-] Exiting..."
	fi
}

function wg_gen_key () {
	local wg_location="/etc/wireguard"

	if [[ ! -d $wg_location ]]; then
		mkdir $wg_location
	fi

    echo -e "\n[*] Initializing wireguard key pair generation..." && $SLEEP
    cd ${wg_location}
    if [[ -f ${wg_location}/privatekey && ${wg_location}/publickey ]]; then
        echo -e "\n[*] Wireguard Keys already exist !"
        echo -e "\n[*] Removing the existing keys and regenerating new one" && sleep 1
        wg genkey | tee privatekey | wg pubkey > publickey
        echo -e "\n[+] Successfully regenerated !" && sleep 1
        echo -e "\n[!] Your wireguard keys"
        echo -e "\n[+] PublicKey: $(cat publickey)"
        echo -e "\n[+] PrivateKey: (hidden)"
        echo -e "\n[*] Copy and and paste the publickey to (https://labs.selfmade.ninja/devices/add) for\n    accessing labs through this VPN\n"
        read -p "[*] If you done this press [Enter] to continue..."
    else
        wg genkey | tee privatekey | wg pubkey > publickey
        echo -e "\n[!] Your wireguard keys"
        echo -e "\n[+] PublicKey: $(cat publickey)"
        echo -e "\n[+] PrivateKey: (hidden)"
        echo -e "\n[*] wirguard keys saved at [${wg_location}]"
        echo -e "\n[*] Copy and and paste the publickey to (https://labs.selfmade.ninja/devices/add) for\n    accessing labs through this VPN\n"
        read -p "[*] If you done this press [Enter] to continue..."
    fi
}

function wg_create_conf(){
	local wg_location="/etc/wireguard"
	local wg_conf="/etc/wireguard/wg0.conf"

	if [[ ! -d ${HOME}/.config ]]; then
		mkdir ${HOME}/.config
		if [[ ! -f $conf_lc ]]; then
			touch $conf_lc
		fi
	fi

    echo -e "\n[*] Creating wireguard configuration ..."
    cd ${wg_location}
    if [[ -f ${wg_location}/wg0.conf ]]; then
        echo -e "\n[*] Wireguard configuration exist !"
        echo -e "\n[*] Writing data to the configuration"
        echo "" > $wg_conf
        echo "[Interface]" > $wg_conf
        echo "PrivateKey = $(cat privatekey)" >> $wg_conf
        echo "Address = ${1}/32" >> $wg_conf
        echo "" >> $wg_conf
        echo "[Peer]" >> $wg_conf
        echo "PublicKey = cm9KJKYKSfXynAgznrH8+8JzYwxdk1Sn62/YWV/amW4=" >> $wg_conf
        echo "AllowedIPs = 172.20.0.0/32" >> $wg_conf
        echo "Endpoint = vpn.selfmade.ninja:44556" >> $wg_conf
        echo "PersistentKeepalive = 30" >> $wg_conf
        
        echo -e "\n[*] Successfully Writed !\n" && sleep 2
    else
        echo -e "\n[*] Creating a new wireguard configuration ..."
        touch $wg_conf
        
        echo "[Interface]" > $wg_conf
        echo "PrivateKey = $(cat privatekey)" >> $wg_conf
        echo "Address = ${1}/32" >> $wg_conf
        echo "" >> $wg_conf
        echo "[Peer]" >> $wg_conf
        echo "PublicKey = cm9KJKYKSfXynAgznrH8+8JzYwxdk1Sn62/YWV/amW4=" >> $wg_conf
        echo "AllowedIPs = 172.20.0.0/32" >> $wg_conf
        echo "Endpoint = vpn.selfmade.ninja:44556" >> $wg_conf
        echo "PersistentKeepalive = 30" >> $wg_conf
        
        echo -e "\n[*] Config generated !" && $SLEEP
        echo -e "\n[*] wg0 conf saved at [$wg_conf]"
    fi 
}

function ask_ip() {
	local iprange IP_R
    
    echo -e "\n[*] Copy the VPN IP of your device shown in labs"
    echo -e "\n[*] Carefully enter the IP. It can't be changed!\n"

	while true; do
	    read -p "[?] Enter the address [Example: 172.20.0.60]: " iprange
	    if [[ "$iprange" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
	        break
	    else
	        echo -e "\n[-] ${RED}Invalid IP! Try again${NC}\n"
	    fi
	done
}

function wg_gen_conf () {
	local wg_location="/etc/wireguard"
	local wg_conf="/etc/wireguard/wg0.conf"
	local check junk iprange

	echo -e "\n[*] Initializing wireguard config generation... "
    while true; do
        if [ -d $wg_location ]; then
            if [ -f $wg_conf ]; then
            	# Check if Address available or not
            	# if [[ $(cat $wg_conf | grep Address | awk -F= '{print $2}') ]]; then
            	if [[ -s $wg_conf ]]; then
            		read -p "[*] want to override the existing configuration ? [Y/n] " junk
            		if [[ $junk == "Y" || $junk == "y" ]]; then
            			echo -e "\n[*] Overriding the existing configuration ..." && sleep 2
            			
					    echo -e "\n[*] Copy the VPN IP of your device shown in labs"
					    echo -e "\n[*] Carefully enter the IP. It can't be changed!\n"

						while true; do
						    read -p "[?] Enter the address [Example: 172.20.0.60]: " iprange
						    if [[ "$iprange" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
						        break
						    else
						        echo -e "\n[-] ${RED}Invalid IP! Try again${NC}\n"
						    fi
						done
            			wg_create_conf $iprange
            			break
            		else
            			break
            		fi
            	else        		
				    echo -e "\n[*] Copy the VPN IP of your device shown in labs"
				    echo -e "\n[*] Carefully enter the IP. It can't be changed!\n"

					while true; do
					    read -p "[?] Enter the address [Example: 172.20.0.60]: " iprange
					    if [[ "$iprange" =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
					        break
					    else
					        echo -e "\n[-] ${RED}Invalid IP! Try again${NC}\n"
					    fi
					done
        			wg_create_conf $iprange
	                break
            	fi
            else
                echo -e "\n[-] '$wg_conf' does not exist!"
                echo -e "\n[*] creating one"
                touch $wg_conf && sleep 1
            fi
        else
            echo -e "[-] '/etc/wireguard/' directory not found !"
            echo -e "\n[*] Creating directory '/etc/wireguard/'\n"
            mkdir $wg_location
        fi
    done
}

function check_wg_status() {
	local check
	
	check=$(ifconfig | grep "wg")

	if [[ $check == "" ]]; then
		echo "[*] Wireguard not seems to be up"
		echo "[*] We are enabling wireguard (wg0) for you"
		wg-quick up wg0
		echo "[!] Wireguard enabled"
	else
		wg-quick down wg0
		wg-quick up wg0
	fi
}

function check_root() {
	if [[ $UID != 0 ]]; then
		echo "[*] Please run as root!"
		exit 1
	fi
}

function main() {

	local wg_conf="/etc/wireguard/wg0.conf"

	check_root
	
	if [[ -f $wg_conf ]]; then
		if [[ -f $conf_lc ]]; then
			_getting_creds_from_config
			check_wg_status
			ssh_labs
		fi
	else
		wg_gen_key
		wg_gen_conf
		_getting_creds_from_config
		echo -e "[*] Try to run again!\n"
	fi
}

main 

## ssh -keygen