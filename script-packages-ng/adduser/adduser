#!/usr/bin/env bash

##########################################################################
# Program	 : adduser
# Description: Interactive front end to 'useradd' which makes 
#		       easier to add user.
# Maintainer : Arcigo <arcigo.linux@gmail.com>
# Version	 : 1.0
##########################################################################
# Usage		 : adduser <Username>
##########################################################################
#

echo="echo -e"
read="read -p"
username="$1"
adduser="/usr/bin/useradd"
chfn="/usr/bin/chfn"

# Check root privileges
if [[ $UID != 0 ]]; then
	$echo "Run as root!"
	$echo "Usage: sudo adduser <new username>"
	exit 1
fi

# Check if argument is passed or not
if [[ $# != 1 ]]; then
	$echo "Usage: adduser <Username>"
	exit 1
fi

# checking purpose
function adding_user() {
	# $echo "Adding user $username ..." && sleep 0.5
	$echo "Adding new group '$username' () ..."
	$echo "Adding new user '$username' () with group '$username' ..." && sleep 0.5
	$echo "Creating home directory '/home/$username' ..."
	$echo "Copying files from '/etc/skel' ..." && sleep 0.5

	useradd -m $username
	passwd $username
	cp -rf /etc/skel/* /home/${username}
}

function _check_user_avail() {
	local user_check

	user_check=$(cut -d: -f1 /etc/passwd | grep "$username")

	if [[ $user_check ]]; then
		$echo "[-] Error, user $username is already used, please choose another."
		$echo "[-] Try again!"
		exit 1
	elif [ ! -z "$( echo $username | grep "^[0-9]" )" ]; then
		$echo "[-] Error, username cannot begin with a number; please choose another"
		$echo "[-] Try again!"
		exit 1
	elif [ ! "$username" = "`echo $username | tr A-Z a-z`" ]; then # adduser does not allow uppercase
		$echo "[-] Error, user '$username' contains illegal characters (uppercase); please choose another"
		$echo "[-] Try again!"
		exit 1
	elif [ ! -z "$( echo $username | grep '\.' )" ]; then
		$echo "[-] Error, user '$username' contains illegal characters (period/dot); please choose another"
		$echo "[-] Try again!"
		exit 1
	else
		$echo "Adding user $username ..." && sleep 0.5
	fi
}

function _change_finger_info() {
	local fullname officephone homephone confirm

	$echo "Changing the user information of '$username'"
	$echo "Enter the new value, or press ENTER for the default"
	$read "        Full Name  []: " fullname
	$read "        Work Phone []: " officephone
	$read "        Home Phone []: " homephone

	$read "Is the information is correct? [Y/n] " confirm
	case $confirm in
		y|Y)
			$chfn "$username" -f $fullname -p $officephone -h $homephone
		;;
		n|N)
		;;
		*)
		;;
	esac
}

_check_user_avail
adding_user
_change_finger_info