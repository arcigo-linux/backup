#!/usr/bin/env bash

# Copyright (C) 2020-2021 Arcigo Linux <arcigo.linux@gmail.com>
# Everyone is permitted to copy and distribute copies of this file under GNU-GPL3

# Dirs
DIR="$(pwd)"
PKGS=(`ls -d */ | cut -f1 -d'/'`)
PKGDIR="$DIR/packages"

# Script Termination
exit_on_signal_SIGINT () {
    { printf "%s\n" "==> Script interrupted !" 2>&1; echo; }
    exit 0
}

exit_on_signal_SIGTERM () {
    { printf "%s\n" "==> Script terminated !" 2>&1; echo; }
    exit 0
}

trap exit_on_signal_SIGINT SIGINT
trap exit_on_signal_SIGTERM SIGTERM

# Build packages
build_pkgs () {
	local pkg

	if [[ ! -d "$PKGDIR" ]]; then
		mkdir -p "$PKGDIR"
	fi

	echo -e "\n==> Started Building Packages"
	for pkg in "${PKGS[@]}"; do
		echo -e "\n -> Building '${pkg}'"
		cd ${pkg} && makepkg -s
		mv *.pkg.tar.zst "$PKGDIR" && rm -rf src pkg

		if [[ "$pkg" == 'adminer' ]]; then
			rm adminer-*.php
		elif [[ "$pkg" == 'brave-bin' ]]; then
			rm -rf src/ *.zip
		elif [[ "$pkg" == 'ckbcomp' ]]; then
			rm -rf pkg/ src/ *.tar.xz
		elif [[ "$pkg" == 'gitahead-bin' ]]; then
			rm -rf pkg/ src/ *.bin
		elif [[ "$pkg" == 'sublime-text-4' ]]; then
			rm -rf pkg/ src/ *.tar.xz
		elif [[ "$pkg" == 'ttf-wps-fonts' ]]; then
			rm -rf pkg/ src/ *.zip
		elif [[ "$pkg" == 'wps-office' ]]; then
			rm -rf pkg/ src/ *.deb
		else
			rm -rf src/ pkg/ *.tar.gz
		fi

		# Verify
		while true; do
			set -- "$PKGDIR"/${pkg}-*
			if [[ -e "$1" ]]; then
				echo -e " -> Package '${pkg}' generated successfully.\n"
				break
			else
				echo -e " -> Failed to build '${pkg}', Exiting...\n"
				exit 1;
			fi
		done
		cd "$DIR"
	done

	echo -e "[*] Want to move packages to repository ?\n" && read -s

	REPO_DIR='../arcigo-repo/x86_64'
	if [[ -d "$REPO_DIR" ]]; then
		mv -f "$PKGDIR"/*.pkg.tar.zst "$REPO_DIR" && rm -r "$PKGDIR"
		echo -e "==> Packages moved to Repository.\n[!] Don't forget to update the database.\n"
	fi
}

# Execute
build_pkgs
